package day1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo4 {

	public static void main(String[] args) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from employee Where empName='Pasha'";

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "1234");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
                String empName = rs.getString(2);

                if ("Pasha".equals(empName) ) {
                    System.out.println("EmpId   : " + rs.getInt(1));
                    System.out.println("EmpName : " + empName);
                    System.out.println("Salary  : " + rs.getDouble(3));
                    System.out.println("Gender  : " + rs.getString("gender"));
                    System.out.println("EmailId : " + rs.getString(5));
                    System.out.println("Password: " + rs.getString(6) + "\n");
                    break; 
                }
            }               
				
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

}
